package ku.sci.cs.myapp;

import static org.junit.Assert.*;

import java.io.IOException;
import java.text.ParseException;

import ku.sci.cs.myapp.server.ScheduleBookController;
import ku.sci.cs.myapp.server.Appointment;

import org.junit.Test;

public class AppointmentBookTest {
	
	//<<<<TEST ADD>>>>
	//<test method AddAppointment >
	@Test
	public void add1AppointmentbookTestTrue() throws AssertionError, ParseException, IOException {
		Appointment appoint = new Appointment("9/04/2015 10:14","JTest","Onetime");
		ScheduleBookController  appointbook = new ScheduleBookController(); 
		appointbook.addAppointment(appoint);
		assertEquals(1,appointbook.getNumofAppointTask());
		
	}
	
	@Test(expected=AssertionError.class)
	public void add1AppointmentbookTestFalse() throws AssertionError, ParseException, IOException {
		Appointment appoint = new Appointment("9/04/2015 10:14","JTest","Onetime");
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addAppointment(appoint);
		assertEquals(2,appointbook.getNumofappoint());
		
	}

	//<test AddTask>
	@Test
	public void addTaskTestTrue() throws AssertionError, ParseException, IOException{ 
		
		Appointment appoint = new Appointment("9/04/2015 10:14","JTest","Onetime");
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addTask("9/04/2015 10:14", "task1");
		appointbook.addTask("9/04/2015 10:14", "task2");
		assertEquals(2,appointbook.getNumoftask());
		
		
	}
	
	@Test(expected = AssertionError.class)
	public void addTaskTestFalse() throws AssertionError, ParseException, IOException{ 
		
		Appointment appoint = new Appointment("9/04/2015 10:14","JTest","Onetime");
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addTask("9/04/2015 10:14", "task1");
		appointbook.addTask("9/04/2015 10:14", "task2");
		assertEquals(4,appointbook.getNumoftask());
		
		
	}
	
	//<<<<TEST GET BY INDEX >>>>
	//<test method getAppointment >
	@Test
	public void getAppointmentTestTrue() throws AssertionError, ParseException, IOException{ 
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addAppointment("9/04/2015 10:14", "Test1","Onetime");
		appointbook.addAppointment("9/04/2015 10:14", "Test2","Daily");
		appointbook.addAppointment("9/04/2015 10:14", "Test3","Weekly");
		assertEquals("Appointment[ date=9/04/2015 10:14 ,description=Test1 ,type=Onetime]",appointbook.getAppoint(1));
		
		
	}
	
	
	@Test(expected = AssertionError.class)
	public void getAppointmentTestFalse() throws AssertionError, ParseException, IOException{ 
		
		Appointment appoint = new Appointment("9/04/2015 10:14","JTest","Onetime");
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addAppointment("9/04/2015 10:14", "Test1","Onetime");
		appointbook.addAppointment("9/04/2015 10:14", "Test2","Daily");
		assertEquals("Appointment[ date=9/04/2015 10:14 ,description=Test1]",appointbook.getAppoint(2));
		
	}
	
	//<<<TEST CHECK>>>
	
	@Test
	public void checkDateTestTrue() throws AssertionError, ParseException, IOException{ 
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addAppointment("09/04/2015 10:14", "Test1","Onetime");
		appointbook.addAppointment("10/04/2015 10:14", "Test2","Daily");
		appointbook.addAppointment("11/04/2015 10:14", "Test3","Weekly");
		
		assertEquals("Appointment[ date=09/04/2015 10:14 ,description=Test1 ,type=Onetime]\n",appointbook.checkDate("09/04/2015 10:14","A"));
	}
	
	
	@Test(expected = AssertionError.class)
	public void checkDateTestFalse() throws AssertionError, ParseException, IOException{ 
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addAppointment("09/04/2015 10:14", "Test1","Onetime");
		appointbook.addAppointment("10/04/2015 10:14", "Test2","Daily");
		appointbook.addAppointment("11/04/2015 10:14", "Test3","Weekly");
		
		assertEquals("Appointment[ date=10/04/2015 10:14 ,description=Test1 ,type=Onetime]\n",appointbook.checkDate("09/04/2015 10:14","A"));
		
	}
	
	//<<<TEST UPDATE>>>
	
	@Test
	public void updateTestTrue() throws AssertionError, ParseException, IOException{ 
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addTask("09/04/2015", "task1");
		appointbook.addTask("10/04/2015", "task2");
		appointbook.addTask("11/04/2015", "task3");
		
		assertEquals(true,appointbook.update("11/04/2015", "true"));
		
	}
	
	@Test(expected = AssertionError.class)
	public void updateTestFalse() throws AssertionError, ParseException, IOException{ 
		ScheduleBookController appointbook = new ScheduleBookController(); 
		appointbook.addTask("09/04/2015", "Task1");
		appointbook.addTask("10/04/2015", "task2");
		appointbook.addTask("11/04/2015", "task3");
		
		assertEquals("Task[ date=11/04/2015 ,description=task1 , status=true]\n",appointbook.update("10/04/2015", "true"));
		
	}


}
