package ku.sci.cs.myapp;

import static org.junit.Assert.*;

import java.text.ParseException;

import ku.sci.cs.myapp.server.Task;

import org.junit.Test;

public class TaskTest {

	@Test
	public void todoListTestTrue() throws ParseException {
		Task task = new Task("9/04/2015","task1","none");
		assertEquals("9/04/2015", task.getDate());
		assertEquals("task1", task.getDesc());
		
		
	}
	
	@Test(expected=AssertionError.class)
	public void todoListTestFalse() throws ParseException,AssertionError {
		Task task = new Task("9/04/2015","task1","-");
		assertEquals("9/04/2015", task.getDate());
		assertEquals("task3", task.getDesc());
		
	}
	

}
