package ku.sci.cs.myapp.server;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**Autor : Narumon Petsiri 5610404444*/

public class File { 
	
	private String filename;
	private BufferedReader buffer;
	private FileReader fileReader;
	private FileWriter filewrite;

	
	public File() throws IOException{
		
		filename = "ScheduleData.txt";
		filewrite = new FileWriter(filename,true);
		
	}
	
	 public void writeData(String acate,String date,String desc,String atype){ 
		
		try {
			
			
			PrintWriter out = new PrintWriter(filewrite);
			out.println("\t" + acate + "\t\t\t" + date + "\t\t\t\t" + desc + "\t\t\t" + atype);
			out.println();
		}finally{
			try {
				filewrite.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	

	}
	
	
	public String readFile(){
		String collect="";
		try {
			//fileReader = new FileReader(filename);
			buffer = new BufferedReader(fileReader);
			String data;
			 
				for (data = buffer.readLine();data!=null;data = buffer.readLine()){
					
					 collect += data+"\n";
				 }
			
		} catch (FileNotFoundException e){
			System.err.println("Cannot read file "+filename);
		}
		catch (IOException e){
			System.err.println("Error reading from file");
		}finally{
			try {
				fileReader.close();
			} catch (IOException e) {
				System.err.println("Error closing files");
			}
		}
		return collect;
	}


}
