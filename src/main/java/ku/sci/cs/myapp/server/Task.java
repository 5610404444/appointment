package ku.sci.cs.myapp.server;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**Autor : Narumon Petsiri 5610404444*/
public class Task extends ScheduleBook{
	
	
	public Task(String adate, String desc, String atype) throws ParseException{
		
		super(adate, desc, atype);
		dateTimeFormat = new SimpleDateFormat("dd/MM/yy");
		try {
			myTime = dateTimeFormat.parse(adate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	

	public String toString(){ 
		return "<Task> Date : "+date+" ,Desc : "+description+"";
	}
	
	public boolean occursOn(String aDate) throws ParseException {
		
		DateFormat compare = new SimpleDateFormat("dd/MM/yy");
		Date compareParse  = compare.parse(aDate);
		
		if(myTime.getDate()==compareParse.getDate()&&myTime.getMonth()==compareParse.getMonth()&&myTime.getYear()==compareParse.getYear()){ 
			return true;
		}
		else { 
			return false;
		}
		
		
	}
	
	public void setStatus(boolean up,String aStatus){ 
		
		if(up==true){ 
			type = aStatus;
		}
		
	}
	
	public String getStatus() {
		return type;
	}

	@Override
	public String getDesc() {
		// TODO Auto-generated method stub
		return description;
	}

	@Override
	public String getDate() {
		// TODO Auto-generated method stub
		return date;
	}



	
}
