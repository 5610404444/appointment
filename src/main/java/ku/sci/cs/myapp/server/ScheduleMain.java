package ku.sci.cs.myapp.server;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ku.sci.cs.myapp.server.ScheduleBookController;

/**Autor : Narumon Petsiri 5610404444*/

public class ScheduleMain {
	
@SuppressWarnings("resource")
public static void main(String args[]) throws ParseException, IOException, SQLException {
		
		
		ApplicationContext bf = new ClassPathXmlApplicationContext("schedulebook.xml");
		ScheduleBookController scheduleBookController = (ScheduleBookController) bf.getBean("scheduleBookController");
		scheduleBookController.start();

		//<<CONSOLE>>
		//AppointmentConsole console =(AppointmentConsole) bf.getBean("console");
		//console.start();

		
	}
	
}
