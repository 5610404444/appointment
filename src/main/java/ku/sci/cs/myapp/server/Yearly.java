package ku.sci.cs.myapp.server;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**Autor : Narumon Petsiri 5610404444*/

public class Yearly extends Appointment{
	
	public Yearly(String date, String desc, String type) throws ParseException {
		super(date,desc,type);
	}
	
	@Override
	public boolean occursOn(String aDate) throws ParseException {
		DateFormat compare = new SimpleDateFormat("dd/MM/yy");
		Date compareParse  = compare.parse(aDate);
		
		if(myTime.getYear()<=compareParse.getYear()  ){ 
			return true;
		}
		else { 
			return false;
		}
	}

}
