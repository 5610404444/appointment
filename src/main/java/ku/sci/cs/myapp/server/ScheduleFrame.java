package ku.sci.cs.myapp.server;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;


/**Autor : Narumon Petsiri 5610404444*/


public class ScheduleFrame extends JFrame{
	
	public static final int FRAME_WIDTH = 350;
	public static final int FRAME_HEIGHT = 600;
	private JRadioButton addApp;
	private JRadioButton addTask;
	private JTextArea result;
	private JRadioButton showApp;
	private JRadioButton showTask;
	private JRadioButton showAll;
	private AbstractButton searchApp;
	private AbstractButton searchAll;
	private JRadioButton searchTask;
	private Button upTask;
	private JLabel header;
	private JPanel displayPanel;

	
	public ScheduleFrame(){ 

		createSchedulePanel();
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setResizable(false);
	
		
	}
	

	public void createSchedulePanel(){ 
		
		Color maroon = new Color(128,0,0);
		displayPanel = new JPanel();
		displayPanel.setLayout(new GridLayout(2, 1));
		header = new JLabel("YOUR SCHEDULE");
		header.setForeground(maroon);
		header.setBounds(20,40,100,100);
		

		result = new JTextArea("");
		result.setBounds(20,60,350,300);
		result.setBackground(Color.white);
		
		
		displayPanel.add(header);
		displayPanel.add(result);
		header.setFont(new Font("Amethyst", Font.BOLD, 25));
		displayPanel.setBackground(Color.white);

		
		JPanel addzonePanel = createAddRadioButton();
		JPanel showzonePanel = createShowRadioButton();
		JPanel searchzonePanel = createSearchRadioButton();
		JPanel updateTaskzonePanel = createTaskSubmitbutton();
		
		JPanel schedulePanel = new JPanel();
		schedulePanel.setLayout(new GridLayout(4, 1));
		schedulePanel.add(addzonePanel);
		schedulePanel.add(showzonePanel);
		schedulePanel.add(searchzonePanel);
		schedulePanel.add(updateTaskzonePanel);
		
		add(schedulePanel,BorderLayout.SOUTH);
		add(displayPanel);
	}
	
	private JPanel createAddRadioButton() {
		addApp = new JRadioButton("Appointment");
		addApp.setBackground(Color.orange);
		//addApp.setFont(new Font("Roboto-Light", Font.BOLD, 12));

		//addApp.addActionListener(new  addAppRadioListener());
		addTask = new JRadioButton("Task");
		//addTask.addActionListener(new  addTaskRadioListener());
		addTask.setBackground(Color.orange);
		

		//listener
		
		ButtonGroup group = new ButtonGroup();
		group.add(addApp);
		group.add(addTask);
				
		JPanel addpanel = new JPanel();
		addpanel.setBackground(Color.orange);

		addpanel.add(addApp);
		addpanel.add(addTask);
		addpanel.setBorder(new TitledBorder(new EtchedBorder(),"ADD SCHEDULE"));
		return addpanel;
	}





	private JPanel createShowRadioButton() {
		showApp = new JRadioButton("Appointment");
		showApp.setBackground(Color.orange);

		//showApp.addActionListener(new showAppRadioListener());
		showTask = new JRadioButton("Task");
		showTask.setBackground(Color.orange);

		//showTask.addActionListener(new showTaskRadioListener());
		showAll = new JRadioButton("ALL");
		showAll.setBackground(Color.orange);

		//showAll.addActionListener(new showAllRadioListener());
		
		
		//listener
		
		ButtonGroup group = new ButtonGroup();
		group.add(showApp);
		group.add(showTask);
		group.add(showAll);
		
		JPanel showpanel = new JPanel();
		showpanel.setBackground(Color.orange);

		showpanel.add(showApp);
		showpanel.add(showTask);
		showpanel.add(showAll);
		showpanel.setBorder(new TitledBorder(new EtchedBorder(),"SHOW SCHEDULE"));
		return showpanel;
	}





	private JPanel createSearchRadioButton() {
		searchApp = new JRadioButton("Appointment");
		searchApp.setBackground(Color.orange);

		//searchApp.addActionListener(new searchAppRadioListener());
		searchTask = new JRadioButton("Task");
		searchTask.setBackground(Color.orange);

		//searchTask.addActionListener(new searchTaskRadioListener());
		searchAll = new JRadioButton("ALL");
		searchAll.setBackground(Color.orange);

		//searchAll.addActionListener(new searchAllRadioListener());
		
		
		//listener
		
		ButtonGroup group = new ButtonGroup();
		group.add(searchApp);
		group.add(searchTask);
		group.add(searchAll);
		
		
		JPanel searchpanel = new JPanel();
		searchpanel.setBackground(Color.orange);

		searchpanel.add(searchApp);
		searchpanel.add(searchTask);
		searchpanel.add(searchAll);
		searchpanel.setBorder(new TitledBorder(new EtchedBorder(),"SEARCH SCHEDULE"));
		return searchpanel;
	}





	private JPanel createTaskSubmitbutton() {
		upTask = new Button("UPDATE TASK");
		upTask.setBackground(Color.WHITE);

		//upTask.addActionListener(new updateRadioListener());
		
		JPanel uptaskpanel = new JPanel();
		uptaskpanel.setBackground(Color.orange);

		uptaskpanel.add(upTask);
		
		uptaskpanel.setBorder(new TitledBorder(new EtchedBorder(),"UPDATE TASK"));
		return uptaskpanel;
	}
	
	public void setAddAppListener(ActionListener lis){ 
		addApp.addActionListener(lis);
	}
	
	public void setAddTaskListener(ActionListener lis){ 
		addTask.addActionListener(lis);
	}
	
	public void setShowAppListener(ActionListener lis){ 
		showApp.addActionListener(lis);
	}
	
	public void setShowTaskListener(ActionListener lis){ 
		showTask.addActionListener(lis);
	}
	
	public void setShowAllListener(ActionListener lis){ 
		showAll.addActionListener(lis);
	}
	public void setSearchAppListener(ActionListener lis){ 
		searchApp.addActionListener(lis);
	}
	
	public void setSearchTaskListener(ActionListener lis){ 
		searchTask.addActionListener(lis);
	}
	
	public void setSearchAllListener(ActionListener lis){ 
		searchAll.addActionListener(lis);
	}
	
	public void setUpdateTaskListener(ActionListener lis){ 
		upTask.addActionListener(lis);
	}


	public void setTextArea(String textshow){
		result.setText(textshow);
		result.setFont(new Font("THSarabunNew",Font.BOLD,12));
	}
	

}
