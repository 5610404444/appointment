package ku.sci.cs.myapp.server;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**Autor : Narumon Petsiri 5610404444*/


public class ServerMain {

	public static void main(String[] args) {
		ApplicationContext bf =  
		         new ClassPathXmlApplicationContext("server-config.xml");

				System.out.println("Server is ready.");
	}

}
