package ku.sci.cs.myapp.server;

import java.text.ParseException;
import java.text.SimpleDateFormat;



/**Autor : Narumon Petsiri 5610404444*/

@SuppressWarnings("serial")
public class Appointment extends ScheduleBook {

	
	public Appointment(String adate, String desc, String atype) throws ParseException{
		
		super(adate, desc, atype);
		dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
		try {
			myTime = dateTimeFormat.parse(adate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public String toString(){ 
		return "<Appointment> Date : "+date+" ,Desc : "+description+"";
	}
	
	public boolean occursOn(String aDate) throws ParseException { 
		return false; 
	}

	public String getDesc() {
		// TODO Auto-generated method stub
		return description;
	}

	public String getDate() {
		// TODO Auto-generated method stub
		return date;
	}

	
	
	

}
