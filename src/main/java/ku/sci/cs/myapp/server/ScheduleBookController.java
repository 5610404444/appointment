package ku.sci.cs.myapp.server;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JOptionPane;



/**Autor : Narumon Petsiri 5610404444*/

public class ScheduleBookController implements ScheduleBookService{
	
	
	private String show;
	private String date;
	private String desc;
	private String timeType;
	private String checkdate;
	private String update;
	private String updateStatus;
	private ScheduleFactory schefact ; 
	private Vector<ScheduleBook> appointBook;
	private Vector<ScheduleBook> taskBook;
	private int numAppointment;
	private int numTask; 
	private String displayData;
	private JdbcSQLiteConnection db;
	private File file;
	private ScheduleFrame scframe; 
	
	public  ScheduleBookController() throws IOException{
		appointBook = new Vector<ScheduleBook>();
		taskBook = new Vector<ScheduleBook>();
		schefact = new ScheduleFactory();
		db = new JdbcSQLiteConnection();
		file = new File();
		
	}

	

	public void addAppointment(String date,String desc,String type) throws ParseException{
		
		
		
		try {
			ScheduleBook sch = null;
			sch = schefact.createSchedule("Appointment", date, desc, type);
			appointBook.add(sch);
			try {
				db.writeDBsqlite("Appointment", date, desc, type);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			file.writeData("Appointment", date, desc, type);
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		
	}
	
	public void addAppointment(Appointment appoint) {
		appointBook.add(appoint);
	}

	


	public void addTask(String date,String desc) throws ParseException{
		
		try {
			ScheduleBook sch = null;
			sch = schefact.createSchedule("Task", date, desc, "-");
			taskBook.add(sch);
			try {
				db.writeDBsqlite("Task", date, desc, "-");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			file.writeData("Task", date, desc, "-");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	//< GET VALUE>
	public int getNumofAppointTask(){
		numAppointment = appointBook.size()+taskBook.size();
		return numAppointment ;
	}
	
	
	
	//<CHECK APPOINTMENT AND TASK>
	public String checkDate(String aDate,String aType) throws ParseException{ 
		String hasDate = "";
		
		if(aType.equalsIgnoreCase("A")){
			for(int i = 0 ; i<=appointBook.size()-1;i++){ 
				if(appointBook.get(i).occursOn(aDate)==true){
					hasDate = appointBook.get(i).toString()+"\n";
				}
			}
			getResultsearch(hasDate);

		}
		
		else if (aType.equalsIgnoreCase("T")){ 
			
			for(int i = 0 ; i<=taskBook.size()-1;i++){ 
				if(taskBook.get(i).occursOn(aDate)==true){
					hasDate = taskBook.get(i).toString()+"\n";
				}
			}
			getResultsearch(hasDate);

			
		}
		else if (aType.equalsIgnoreCase("B")){ 
			
			for(int i = 0 ; i<=appointBook.size()-1;i++){ 
				if(appointBook.get(i).occursOn(aDate)==true){
					hasDate += appointBook.get(i).toString()+"\n";
				}
			}
			for(int i = 0 ; i<=taskBook.size()-1;i++){ 
				if(taskBook.get(i).occursOn(aDate)==true){
					hasDate += taskBook.get(i).toString()+"\n";
				}
			}
			
			getResultsearch(hasDate);
			
		}
		
		

		return hasDate;
		
	}
	

	@Override
	public int getNumofappoint() {
		numAppointment = appointBook.size();
		return numAppointment;
	}

	@Override
	public int getNumoftask() {
		numTask = taskBook.size();
		return numTask;
	}

	@Override
	public String getAppoint(int index) {
		displayData = appointBook.get(index)+"";
		return displayData;
	}

	@Override
	public String getTask(int index) {
		// TODO Auto-generated method stub
		displayData = taskBook.get(index)+"";
		return displayData;
	}

	
	//<UPDATE>
	
	public boolean update(String aDate,String status) throws ParseException{
		
		boolean canup = false;
		for(int i = 0 ; i<=taskBook.size()-1;i++){ 
			if(taskBook.get(i).occursOn(aDate)==true){
				canup =  true; 
				((Task) taskBook.get(i)).setStatus(canup, status);
			}
			else {
				canup =  false;
			}
		}
		scframe.setTextArea("Task on date : "+update+"   Done!");

		
		return canup;
		
	}
	
	public void start(){ 
		scframe = new ScheduleFrame();
		scframe.setVisible(true);
		addapplis = new addAppRadioListener();
		addtasklis = new addTaskRadioListener();
		showapplis = new showAppRadioListener();
		showtasklis = new showTaskRadioListener();
		showalllis = new showAllRadioListener();
		searchapplis = new searchAppRadioListener();
		searchtasklis = new searchTaskRadioListener();
		searchalllis = new searchAllRadioListener();
		uplisl = new updateRadioListener();
		
		scframe.setAddAppListener(addapplis);
		scframe.setAddTaskListener(addtasklis);
		scframe.setShowAppListener(showapplis);
		scframe.setShowTaskListener(showtasklis);
		scframe.setShowAllListener(showalllis);
		scframe.setSearchAppListener(searchapplis);
		scframe.setSearchTaskListener(searchtasklis);
		scframe.setSearchAllListener(searchalllis);
		scframe.setUpdateTaskListener(uplisl);
		
	}

	
	public void getResultsearch(String hasdate){ 
		scframe.setTextArea(hasdate);

	}
	
	public void showSchedule(String type){
		
		show = toString(type);
		scframe.setTextArea(show);
		
	
	
	}
	
	
	public String toString(String type){
		displayData = "";
		if(type.equalsIgnoreCase("A")){ 
			for(int i = 0; i<appointBook.size();i++){ 
				displayData +=appointBook.get(i).toString()+"\n";
			}
		}
		else if(type.equalsIgnoreCase("T")){ 
			for(int i = 0; i<taskBook.size();i++){ 
				displayData +=taskBook.get(i).toString()+"\n";
			}
		}
		
		else if(type.equalsIgnoreCase("B")){ 
			for(int i = 0; i<appointBook.size();i++){ 
				displayData +=appointBook.get(i).toString()+"\n";
			}
			for(int i = 0; i<taskBook.size();i++){ 
				displayData +=taskBook.get(i).toString()+"\n";
			}
		}
		return displayData; 
	}
	
	
class addAppRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			date =JOptionPane.showInputDialog(new JFrame(),"Enter Date  : \nex.dd/mm/yy HH:mm");
			desc =JOptionPane.showInputDialog(new JFrame(),"Enter Description : ");
			timeType =JOptionPane.showInputDialog(new JFrame(),"Enter Type <Onetime,Daily,Weekly,Monthly,Yearly> : ");
			try {
		
				addAppointment(date, desc, timeType);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}

	}
	
	class addTaskRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			date =JOptionPane.showInputDialog(new JFrame(),"Enter Date  : \nex.dd/mm/yy");
			desc =JOptionPane.showInputDialog(new JFrame(),"Enter Description : ");
			try {
				addTask(date, desc);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	class showAppRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			showSchedule("A");
			
		}
	}
	
	class showTaskRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			showSchedule("T");

		}
	}
	class showAllRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			showSchedule("B");
			
			
		}
	}
	
	class searchAppRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			
			checkdate =JOptionPane.showInputDialog(new JFrame(),"Enter Date  : \nex.dd/mm/yy");
			try {
				
				checkDate(checkdate, "A");

			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	class searchTaskRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			checkdate =JOptionPane.showInputDialog(new JFrame(),"Enter Date  : \nex.dd/mm/yy");
			try {
			
				checkDate(checkdate, "T");

			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	
	class searchAllRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			checkdate =JOptionPane.showInputDialog(new JFrame(),"Enter Date  : \nex.dd/mm/yy");
			try {
				checkDate(checkdate,"B");

			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}
	}
	
	class updateRadioListener implements ActionListener{

		

		@Override
		public void actionPerformed(ActionEvent e) {
			update =JOptionPane.showInputDialog(new JFrame(),"Enter Date  : \nex.dd/mm/yy");
			updateStatus =JOptionPane.showInputDialog(new JFrame(),"Enter status of task (true/false) :");
			try {
				update(update, updateStatus);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}	
			
		}
	}
	
	ActionListener addapplis; 
	ActionListener addtasklis; 
	ActionListener showapplis;
	ActionListener showtasklis; 
	ActionListener showalllis; 
	ActionListener searchapplis; 
	ActionListener searchtasklis; 
	ActionListener searchalllis; 
	ActionListener uplisl;

	
	


}
