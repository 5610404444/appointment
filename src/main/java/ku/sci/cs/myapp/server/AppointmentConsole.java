package ku.sci.cs.myapp.server;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**Autor : Narumon Petsiri 5610404444*/

public class AppointmentConsole {
	
	private BufferedReader str;
	private ScheduleBookController allBook;
	private String scheduleType = ""; 
	private String timeType = "";
	private String filewritename;
	private FileWriter fileWrite;
	
	
	public AppointmentConsole(ScheduleBookController aAppointBook){ 
		str = new BufferedReader(new InputStreamReader(System.in));
		allBook = aAppointBook;
		filewritename = "ScheduleData.txt";
		
		
	}
	
	@SuppressWarnings("unused")
	public void start() throws ParseException, IOException{ 
		

		
		
		fileWrite = new FileWriter(filewritename,true);
		PrintWriter out = new PrintWriter(fileWrite);
		
		System.out.println("<<Please select number of items>>");
		System.out.println("1.ADD \n2.SHOW\n3.SUM \n4.GET BY NUMBER\n5.SEARCH\n6.UPDATE TASK\n7.EXIT PROGRAM");
		String select = str.readLine();
	
		while (select.equalsIgnoreCase("1")||select.equalsIgnoreCase("2")||select.equalsIgnoreCase("3")
				||select.equalsIgnoreCase("4")||select.equalsIgnoreCase("5")||select.equalsIgnoreCase("6")||select.equalsIgnoreCase("7")){
			if(select.equalsIgnoreCase("1")){ 
				System.out.println("<<< Add >>>");
				System.out.println("<< Select type (Apppointment(A)/Task(T)) : ");
				scheduleType = str.readLine();
				
				if (scheduleType.equalsIgnoreCase("A")){ 
					out.print("\tAppointment\t\t\t");
					System.out.print("Date: ");
					String strdate = str.readLine();
					DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
					try {
						Date myTime = dateTimeFormat.parse(strdate);
						out.print(strdate+"\t\t\t\t");
					} catch (ParseException e) {
						System.err.println("Please enter the correct format (dd/MM/yy HH:mm)");
						System.out.print("Date: ");
						strdate = str.readLine();
						dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
						Date myTime = dateTimeFormat.parse(strdate);
						
					}
					System.out.print("Description: ");
					String desc = str.readLine();
					out.print(desc+"\t\t\t\t");
					System.out.print("Type <Onetime,Daily,Weekly,Monthly,Yearly> : ");
					timeType = str.readLine();
					out.print(timeType);
					allBook.addAppointment(strdate, desc,timeType);
					out.println();
				}
				
				else if(scheduleType.equalsIgnoreCase("T")){ 
					System.out.print("Date: ");
					out.print("\tTask\t\t\t\t");
					String strdate = str.readLine();
					DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy");
					try {
						Date myTime = dateTimeFormat.parse(strdate);
						out.print(strdate+"\t\t\t\t");
					} catch (ParseException e) {
						System.err.println("Please enter the correct format (dd/MM/yy)");
						System.out.print("Date: ");
						strdate = str.readLine();
						dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
						Date myTime = dateTimeFormat.parse(strdate);
					}
					System.out.print("Description: ");
					String desc = str.readLine();
					out.println(desc+"\t\t\t");
					allBook.addTask(strdate, desc);
				}
				
				out.flush();
				
			}
			
			else if (select.equalsIgnoreCase("2")){
				System.out.println("<<< SHOW >>>");
				System.out.println("<< Select type (Apppointment(A)/Task(T)/Both(B) : ");
				scheduleType = str.readLine();
				
				if(scheduleType.equalsIgnoreCase("A")){ 
					System.out.println(allBook.toString(scheduleType));
				}
				else if (scheduleType.equalsIgnoreCase("T")){ 
					System.out.println(allBook.toString(scheduleType));
				}
				else if (scheduleType.equalsIgnoreCase("B")){ 
					System.out.println(allBook.toString(scheduleType));
				}
				
				
				
			}
			else if (select.equalsIgnoreCase("3")){
				System.out.println("<<< SUM >>>");
				System.out.println("<< Select type (Apppointment(A)/Task(T)/Both(B) : ");
				scheduleType = str.readLine();
				
				if(scheduleType.equalsIgnoreCase("A")){ 
					System.out.println("Summations of all appointment is : "+allBook.getNumofappoint());
				}
				else if (scheduleType.equalsIgnoreCase("T")){ 
					System.out.println("Summations of all appointment is : "+allBook.getNumoftask());
				}
				else if (scheduleType.equalsIgnoreCase("B")){ 
					System.out.println("Summations of all appointment is : "+allBook.getNumofAppointTask());
				}
				
				
			}
			else if (select.equalsIgnoreCase("4")){
				System.out.println("<<< GET BY NUMBER >>>");
				System.out.println("<< Select type (Apppointment(A)/Task(T) : ");
				scheduleType = str.readLine();
				
				if(scheduleType.equalsIgnoreCase("A")){ 
					System.out.println("Please enter your index number : ");
					String indexstr = str.readLine();
					int index = Integer.parseInt(indexstr);
					System.out.println("You select appointment number : "+index);
					System.out.println(allBook.getAppoint(index));
				}
				if(scheduleType.equalsIgnoreCase("T")){ 
					System.out.println("Please enter your index number : ");
					String indexstr = str.readLine();
					int index = Integer.parseInt(indexstr);
					System.out.println("You select task number : "+index);
					System.out.println(allBook.getTask(index));
				}
				
				
			}
			else if(select.equalsIgnoreCase("5")){ 
				System.out.println("<<< SEARCH >>>");
				System.out.println("<< Select type (Apppointment(A)/Task(T)/Both(B) : ");
				scheduleType = str.readLine();
				
				if(scheduleType.equalsIgnoreCase("A")){
					System.out.println("Enter date : ");
					String checkdate = str.readLine(); 
					System.out.println(allBook.checkDate(checkdate,scheduleType));
				}
				
				else if (scheduleType.equalsIgnoreCase("T")){ 
					System.out.println("Enter date : ");
					String checkdate = str.readLine(); 
					System.out.println(allBook.checkDate(checkdate,scheduleType));
				}
				
				else if (scheduleType.equalsIgnoreCase("B")){ 
					System.out.println("Enter date : ");
					String checkdate = str.readLine(); 
					System.out.println(allBook.checkDate(checkdate,"B"));
				}
				
				
			}
			else if (select.equalsIgnoreCase("6")){
				System.out.println("<<< UPDATE >>>");
				System.out.println("Enter date : ");
				String udate = str.readLine(); 
				System.out.println("Enter status : ");
				String updateStatus = str.readLine();
				allBook.update(udate, updateStatus);			
			}
			else if (select.equalsIgnoreCase("7")){
				System.out.println("You already exit this program.\nThank You ");
				
				break;
			
			}
			System.out.println("------------------------------------");
			System.out.println("<<Please select number of items>>");
			System.out.println("1.ADD \n2.SHOW\n3.SUM \n4.GET BY NUMBER\n5.SEARCH\n6.UPDATE TASK\n7.EXIT PROGRAM");
			select = str.readLine();
		}
		
		
		
		
		 
		
	}
	


}
