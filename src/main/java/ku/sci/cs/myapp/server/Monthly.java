package ku.sci.cs.myapp.server;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**Autor : Narumon Petsiri 5610404444*/

@SuppressWarnings("serial")
public class Monthly extends Appointment{
	
	public Monthly(String date, String desc, String type) throws ParseException {
		super(date,desc,type);
	}
	

	@Override
	public boolean occursOn(String aDate) throws ParseException {
		DateFormat compare = new SimpleDateFormat("dd/MM/yy");
		Date compareParse  = compare.parse(aDate);
		
		if(myTime.getMonth()<compareParse.getMonth()){ 
			return true;
		}
		else { 
			return false;
		}
	}

}
