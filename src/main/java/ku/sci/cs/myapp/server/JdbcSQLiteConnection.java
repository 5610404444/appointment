package ku.sci.cs.myapp.server;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;

/**Autor : Narumon Petsiri 5610404444*/


public class JdbcSQLiteConnection {
	
	
	private Connection conn;
	private Statement statement;
	private String result;
	
	public JdbcSQLiteConnection() {
		
		try {
			 Class.forName("org.sqlite.JDBC");
	         String dbURL = "jdbc:sqlite:schedulebooks.db";
	         conn = DriverManager.getConnection(dbURL);
	         statement = conn.createStatement();
			
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	
	public void readDBsqlite() throws SQLException, ParseException, IOException{
		
		ScheduleBookController book = new ScheduleBookController();
		
		if(conn != null){
			System.out.println("Connected to the database....");
			
			//display database information
			DatabaseMetaData dm = (DatabaseMetaData)conn.getMetaData();
			System.out.println("Driver name: "+dm.getDriverName());
			System.out.println("Product name: "+dm.getDatabaseProductName());
			
			//execute SQL statement
			System.out.println("----- Data in book table -----");
			
			String query = "select*from scheduleBooks";
			Statement statement = conn.createStatement();
			ResultSet resultSet = statement.executeQuery(query);
			
			System.out.println("<<ALL SCHEDULE BOOK>>");
			while(resultSet.next()){
				String cate = resultSet.getString(1);
				String date = resultSet.getString(2);
				String desc = resultSet.getString(3);
				String type = resultSet.getString(4);
				
				
				if(cate.equalsIgnoreCase("Appointment")){
					
					book.addAppointment(date,desc,type);
					
				}
				else if(cate.equalsIgnoreCase("Task")){
					
					book.addTask(date, desc);
					
				}
				
				
				
			}
			int numapp  = book.getNumofappoint();
			for(int i = 1 ; i<numapp;i++){ 
				
				result += "";
				result += book.getAppoint(i);
				result += "\n";
				
			}
			
			int numtask  = book.getNumoftask();
			for(int i = 1 ; i<numtask;i++){ 
				
				
				//System.out.println(book.getTask(i));
				result += book.getTask(i);
				result += "\n";
				
			}
			System.out.println(book.toString("B"));
		
		}
		
		conn.close();
		
	}
	
	public void writeDBsqlite(String aCate,String aDate,String aDes,String aType) throws SQLException{
		String categories = aCate;
		String date = aDate;
		String desc = aDes;
		String type = aType;
		
		String sql = "INSERT INTO ScheduleBooks(categories,date,description,type)"
	    		+ "VALUES('"+categories+"','"+date+"','"+desc+"','"+type+"');";
	    statement.executeUpdate(sql);
	}

}
