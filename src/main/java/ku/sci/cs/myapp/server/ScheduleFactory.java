package ku.sci.cs.myapp.server;
/**Autor : Narumon Petsiri 5610404444*/

public class ScheduleFactory {
	public ScheduleBook createSchedule(String modelSch,
			String adate, String desc, String atype) throws java.text.ParseException {
		ScheduleBook schbook = null;
		switch (modelSch) {
		case "Appointment":
			
			if(atype.equalsIgnoreCase("Onetime")){
				schbook = new Onetime(adate, desc,atype);
			}
			if(atype.equalsIgnoreCase("Daily")){
				schbook = new Daily(adate, desc,atype);

			}
			if(atype.equalsIgnoreCase("Weekly")){
				schbook = new Weekly(adate, desc,atype);

			}
			if(atype.equalsIgnoreCase("Monthly")){
				schbook = new Monthly(adate, desc,atype);

			}
			if(atype.equalsIgnoreCase("Yearly")){
				schbook = new Yearly(adate, desc,atype);

			}
			
				
			break;

		case "Task":
			schbook = new Task(adate, desc,atype);
			break;

		default:
			// throw some exception
			break;
		}
		return schbook;
	}
	
	

}
