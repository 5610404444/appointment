package ku.sci.cs.myapp.server;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**Autor : Narumon Petsiri 5610404444*/

@SuppressWarnings("serial")
public abstract class ScheduleBook implements Serializable {
	
	protected String description;
	protected String date;
	protected DateFormat dateTimeFormat ;
	protected Date myTime; 
	protected String type;
	public ScheduleBook(String adate,String adesc,String atype){
		this.description = adesc;
		this.date = adate;
		this.type = atype;
	}
	
	public abstract String toString();
	public abstract boolean occursOn(String aDate) throws ParseException;
	public abstract String getDesc();
	public abstract String getDate();

}
