package ku.sci.cs.myapp.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
/**Autor : Narumon Petsiri 5610404444*/

public class ClientMain {
	public static void main(String[] args) {

		@SuppressWarnings("resource")
		ApplicationContext bf =  
				new ClassPathXmlApplicationContext("client-config.xml");

		ScheduleBookService service = (ScheduleBookService) bf.getBean("scheService");
		service.start();

	}
}
