package ku.sci.cs.myapp.client;

import java.text.ParseException;

/**Autor : Narumon Petsiri 5610404444*/

public interface ScheduleBookService { 
	
	public void start(); 
	public void addAppointment(String date,String desc,String type) throws ParseException;
	public void addTask(String date,String desc) throws ParseException;
	public int getNumofAppointTask();
	public int getNumofappoint(); 
	public int getNumoftask(); 
	public String getAppoint(int index); 
	public String getTask(int index); 
	public String checkDate(String aDate,String aType) throws ParseException;
	public boolean update(String aDate,String status) throws ParseException;
	public String toString(String type);
	
	

	

}